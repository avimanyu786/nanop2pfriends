# Welcome To Nano P2P Friends

NanoP2PFriends is a community-driven initiative aimed at enabling  peer-to-peer exchange of [Nano](https://www.nano.org/) within the Indian community. The app utilizes Nano public wallet addresses and UPI (Unified Payments Interface) [Virtual Payment Addresses](https://cleartax.in/s/vpa-virtual-payment-address) (VPAs) to enable secure and efficient transactions.

No intermediaries or third parties are involved in the exchange process. The app is meant to encourage personal interaction among friends through the use of QR codes for peer-to-peer transactions. It is designed to be simple and easy to use, making it accessible  to everyone in the Nano community in India.

The NanoP2PFriends app is user-friendly and enables quick and secure peer-to-peer Nano transactions. Please note that the maximum amount of INR that can be spent per day is limited to [UPI transaction limits](https://teqip.in/upi-payment-daily-limit.html), and you may use the app to exchange Nano received through [sendnano.com](https://www.sendnano.com/) [on Twitter](https://www.twitter.com/sendnano), or [WeNano](https://play.google.com/store/apps/details?id=com.tipanano.WeNanoLight), which cannot be used to buy local items such as food through PhonePe, PayTM, or GooglePay due to restrictions by Indian exchanges.

# Prerequisites

- An Android smartphone
- A Nano wallet (e.g.,  [Natrium](https://play.google.com/store/apps/details?id=co.banano.natriumwallet))
- A UPI-enabled wallet (e.g., [PhonePe](https://play.google.com/store/apps/details?id=com.phonepe.app))
- Internet access

# Features

- Conversion of Nano to INR with real-time updates (maximum of 10 requests per minute)
- Adjustment of INR amount based on mutual and in-person peer to peer agreement

# Getting started

<img src="https://gitlab.com/avimanyu786/nanop2pfriends/-/raw/master/nanop2pfriends.jpeg" width="600" height="800">


The NanoP2PFriends app is user-friendly and enables quick and secure peer-to-peer Nano transactions.

## For Friend 1 (Selling Nano)

1. [Download](https://gitlab.com/avimanyu786/nanop2pfriends/uploads/1a795f89639faa63bc264846491a990e/Nano_P2P_Friends.apk) and install the NanoP2PFriends app.
2. Open the app and enter the amount of Nano you wish to sell in the second field.
3. Tap "Convert to INR" to calculate the equivalent value in INR. The converted amount will be displayed in the UPI section.
4. Enter your UPI VPA in the UPI section and tap "Receive INR."
5. A QR code will be generated. Show this QR code to Friend 2.
6. Once Friend 2 has sent the payment to your UPI VPA, you may send the agreed amount of Nano to Friend 2's Nano wallet.

## For Friend 2 (Buying Nano)

1. [Download](https://gitlab.com/avimanyu786/nanop2pfriends/uploads/1a795f89639faa63bc264846491a990e/Nano_P2P_Friends.apk) and install the NanoP2PFriends app.
2. Open your UPI-enabled wallet app and scan the QR code provided by Friend 1.
3. Send the agreed amount of INR to Friend 1's UPI VPA.
4. Open the NanoP2PFriends app and enter your Nano wallet address in the first field.
5. Tap "Receive Nano" to generate a Nano QR code.
6. Show the Nano QR code to Friend 1.
7. Once Friend 1 has sent the agreed amount of Nano to your wallet, the transaction is complete.

And that's it! This process allows for the exchange of small to moderate amounts, promoting decentralization in India. Additionally, this is an opportunity to determine which payment method is faster, Nano or UPI.

# Verify Hash

To verify that you have obtained the correct APK package, you may use the provided SHA-256 hash to confirm its authenticity with [Checksum Calculator](https://play.google.com/store/apps/details?id=com.raspreet.checksumcalculator):

```
47b48ca243d219adc1bd75d4afe1aca7bbf31e5803e77196a67028a932195e5a
```

**Disclaimer**: Please note that this app is not yet open source as it is in the early-access, pre-alpha stage of development. Also, please be cautious when using peer-to-peer exchange apps and only transact with individuals or organizations you trust. Always take steps  to secure your personal information and Nano holdings. That is why it's called NanoP2PFriends.

